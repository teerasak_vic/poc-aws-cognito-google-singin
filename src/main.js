// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import AsyncComputed from 'vue-async-computed'

import App from './components/App'
import router from './router'

import { initAuth, onSessionChange, getIdToken } from './services/auth'
import * as AWS from './services/aws'

Vue.use(AsyncComputed)
Vue.config.productionTip = false

onSessionChange((signedIn) => {
  if (signedIn) {
    AWS.setCredentials(getIdToken())
  } else {
    AWS.destroy()
  }
})

initAuth().then(() => {
  /* eslint-disable no-new */
  new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: { App }
  })
})
