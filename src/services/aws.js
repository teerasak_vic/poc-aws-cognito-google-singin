import AWS from 'aws-sdk/global'

const ROLE_ARN = 'arn:aws:cognito-idp:ap-southeast-1:055846995890:userpool/ap-southeast-1_HtYqvaIK4'
const AWS_REGION = 'ap-southeast-1'

export function destroy () {
  console.log('destroy')
}

export function setCredentials (idToken) {
  AWS.config.credentials = new AWS.WebIdentityCredentials({
    RoleArn: ROLE_ARN,
    WebIdentityToken: idToken
  })

  console.log('set credential')
}

AWS.config.update({
  region: AWS_REGION,
  logger: console
})
