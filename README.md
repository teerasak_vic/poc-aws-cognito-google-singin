# vue-aws

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## Todos 

[ ] signup ~ save user information into AWS-Cognito
[ ] signin ~ update loged-in date in AWS-Cognito
[ ] authorize ~ save user data into internal user pool and external user pool
